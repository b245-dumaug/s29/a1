db.users.find(
  {
    $or: [{ firstName: { $regex: /s/i } }, { lastName: { $regex: /d/i } }],
  },
  { _id: 0, firstName: 1, lastName: 1 }
);

db.users.find({
  $and: [{ department: 'HR' }, { age: { $gte: 70 } }],
});

db.users.find({
  $and: [{ firstName: { $regex: /e/i } }, { age: { $lte: 30 } }],
});
